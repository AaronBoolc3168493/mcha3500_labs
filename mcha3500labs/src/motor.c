#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "motor.h"

/* Structures */
static GPIO_InitTypeDef GPIO_InitStructure;		// PWM Pin Struct
static TIM_HandleTypeDef htim3; 				// PWM Timer Struct
static TIM_OC_InitTypeDef sConfigPWM;			// PWM Configuration Struct

static GPIO_InitTypeDef GPIO_InitStruct;		// Encoder Pin Struct

/* Variables and Counters */
int32_t enc_count = 0;



// HAL Sections 29.2.2, 65.1
void motor_PWM_init(void)
{
	/* Enable TIM3 clock */
	__TIM3_CLK_ENABLE(); 
	
	/* Enable GPIOA clock */
	__GPIOA_CLK_ENABLE();
		
	
	/* Initialise PA6 with:
	- Pin 6
	- Alternate function push-pull mode
	- No pull
	- High frequency
	- Alternate function 2 - Timer 3*/
	GPIO_InitStructure.Pin = GPIO_PIN_6;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF2_TIM3;
	
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	
	/* Initialise timer 3 with:
	- Instance TIM3
	- Prescaler of 1
	- Counter mode up
	- Timer period to generate a 10kHz signal
	- Clock division of 0 */
	htim3.Instance = TIM3;
    htim3.Init.Prescaler = 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 10000;
	htim3.Init.ClockDivision = 0;
	
	HAL_TIM_PWM_Init(&htim3);
	
	
	
	/* Configure timer 3, channel 1 with:
	- Output compare mode PWM1
	- Pulse = 0
	- OC polarity high
	- Fast mode disabled */
	sConfigPWM.OCMode = TIM_OCMODE_PWM1;
	sConfigPWM.Pulse = 0;
	sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigPWM.OCFastMode = TIM_OCFAST_ENABLE;

	HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigPWM, TIM_CHANNEL_1);


	/* Set initial Timer 3, channel 1 compare value */
	// Duty Cycle: 2500 = 25% -> 10000 = 100%
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 2500);
	
	
	/* Start Timer 3, channel 1 */
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	
}





void motor_encoder_init(void)
{
	/* Enable GPIOC clock */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	
	/* Initialise PC0, PC1 with:
	- Pin 0|1
	- Interrupt rising and falling edge
	- No pull
	- High frequency */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


	/* Set priority of external interrupt lines 0,1 to 0x0f, 0x0f
	To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
	CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
	//EXTI0_IRQn                  = 6,      /*!< EXTI Line0 Interrupt*/
	//EXTI1_IRQn                  = 7,      /*!< EXTI Line1 Interrupt*/     
	HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);


	/* Enable external interrupt for lines 0, 1 */
	HAL_NVIC_EnableIRQ(EXTI0_IRQn); 
	HAL_NVIC_EnableIRQ(EXTI1_IRQn); 
}


// Pin 0
void EXTI0_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	 if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	 {
		enc_count ++; 
	 }
	 else 
	{
		enc_count --; 
	}
	
	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}


// Pin 1
void EXTI1_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	 if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	 {
		enc_count --; 
	 }
	 else 
	{
		enc_count ++; 
	}
	
	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}


int32_t motor_encoder_getValue(void)
{
	return enc_count;
}